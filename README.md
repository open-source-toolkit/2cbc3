# UMP Pro 2.0.3 - Unity 通用媒体框架插件

## 简介
UMP Pro 2.0.3 是一个适用于 Unity 的通用媒体框架插件，基于 Video LAN Codec (libVLC) 原生库。该插件旨在为 Unity 开发者提供强大的媒体播放功能，支持多种音视频格式，并具备高度可定制性。

## 主要功能
- **多格式支持**：支持播放多种音视频格式，包括但不限于 MP4、AVI、MKV、FLV 等。
- **高性能**：基于 libVLC 原生库，提供高效的媒体解码和播放性能。
- **可定制性**：提供丰富的 API 接口，允许开发者根据需求自定义播放器行为。
- **跨平台**：支持 Windows、macOS、Linux、Android 和 iOS 等多个平台。

## 安装与使用
1. **下载资源文件**：从本仓库下载 `UMP Pro 2.0.3` 资源文件。
2. **导入 Unity**：将下载的资源文件导入到你的 Unity 项目中。
3. **配置播放器**：根据项目需求，配置 UMP Pro 播放器，并调用相关 API 进行播放控制。

## 示例代码
以下是一个简单的示例代码，展示如何在 Unity 中使用 UMP Pro 播放视频：

```csharp
using UnityEngine;
using UMP;

public class VideoPlayerExample : MonoBehaviour
{
    private UniversalMediaPlayer mediaPlayer;

    void Start()
    {
        // 初始化媒体播放器
        mediaPlayer = gameObject.AddComponent<UniversalMediaPlayer>();

        // 设置视频路径
        mediaPlayer.Path = "path/to/your/video.mp4";

        // 开始播放
        mediaPlayer.Play();
    }

    void Update()
    {
        // 检查播放状态
        if (mediaPlayer.IsPlaying)
        {
            Debug.Log("视频正在播放");
        }
    }
}
```

## 贡献
我们欢迎社区的贡献！如果你有任何改进建议或发现了 bug，请提交 Issue 或 Pull Request。

## 许可证
本项目采用 MIT 许可证。详细信息请参阅 [LICENSE](LICENSE) 文件。

## 联系我们
如果你有任何问题或需要进一步的帮助，请通过以下方式联系我们：
- 邮箱：support@ump-plugin.com
- 官方网站：[https://ump-plugin.com](https://ump-plugin.com)

---

感谢你使用 UMP Pro 2.0.3！希望它能为你的 Unity 项目带来便利和高效的多媒体播放体验。